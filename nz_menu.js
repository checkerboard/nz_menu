var nzMenu = function() {
    'use strict';
    /*this provides the functionality of the menu.
    this is independent of displaying or hiding the menu itself*/
    var addEventAll = function(selector, action, cb) {
        var expand = document.querySelectorAll(selector);
        expand = [].slice.call(expand);
        expand.forEach(function (ele) {
            ele.addEventListener(action, cb, true);
        }, this);
    };
    var classListAll = function(eles, action, classes) {
        eles = [].slice.call(eles);
        eles.forEach(function (ele) {
            ele.classList[action](classes);
        }, this);
    };
    var selectClosest = function(ele, selector) {
        var matches = ele.matches || ele.webkitMatchesSelector || ele.mozMatchesSelector || ele.msMatchesSelector;
        while (ele) {
            if (matches.call(ele, selector)) {
                break;
            }
            ele = ele.parentElement;
        }
        return ele;
    };
    var selectAllSiblings = function(item) {
        var parent = item.parentElement;
        var eles = parent.querySelectorAll(item.tagName.toLowerCase());
        return [].slice.call(eles).filter(function(ele) {
            return parent === ele.parentElement && ele !== item;
        });
    };
    return {
        init: function(menu) {
            /*pass in one nested list*/
            this.menu = menu;
            this.menuActive = false;
            this.primary = menu.querySelector('.nz_level_0');
            /*this is the collapse button for the submenus*/
            this.collapse = document.createElement('a');
            this.collapse.innerHTML = '[';
            this.collapse.className = 'nz_collapse';
            this.primary.appendChild(this.collapse);
            this.events();
        },
        events: function() {
            addEventAll.call(this, '.nz_expand', 'click', this.showSubMenu.bind(this));
            this.collapse.addEventListener('click', this.hideSubMenu.bind(this), true);
            this.primary.addEventListener('transitionend', this.transitionEndPrimary.bind(this), true);
            addEventAll.call(this, '.nz_level_1', 'transitionend', this.transitionEndSecondary.bind(this));
        },
        showSubMenu: function(e) {
            this.menuActive = true;
            this.activeParent = selectClosest(e.target, '.nz_list');
            this.activetListItem = selectClosest(e.target, 'li');
            this.activeSecondary = this.activetListItem.querySelector('.nz_level_1');
            var siblings = selectAllSiblings(this.activetListItem);
            siblings = [].slice.call(siblings);
            siblings.forEach(function(ele) {
                classListAll(ele.querySelectorAll('ul'), 'add', 'nz_item_hide');
            }, this);
            this.activeParent.classList.add('nz_sub_active_0');
            this.activeSecondary.classList.add('nz_sub_active_1');
            this.collapse.classList.add('nz_collapse_active_1');
            //we don't want scrollbars during transition
            this.menu.classList.add('nz_menu_transitioning');
        },
        hideSubMenu: function() {
            this.menuActive = false;
            this.activeParent.classList.remove('nz_sub_active_0');
            this.activeSecondary.classList.remove('nz_sub_active_1');
            this.collapse.classList.remove('nz_collapse_active_1');
            var siblings = selectAllSiblings(this.activetListItem);
            classListAll(siblings, 'remove', 'nz_item_hide');
        },
        transitionEndPrimary: function() {

        },
        transitionEndSecondary: function() {
            this.menu.classList.remove('nz_menu_transitioning');
            var siblings = selectAllSiblings(this.activetListItem);
            siblings = [].slice.call(siblings);
            if (this.menuActive === false) {
                siblings.forEach(function(ele) {
                    classListAll(ele.querySelectorAll('ul'), 'remove', 'nz_item_hide');
                }, this);
                
            }
            else{
                classListAll(siblings, 'add', 'nz_item_hide');
            }
        }
    };
};
var nzMenuToggle = function() {
    'use strict';
    /*this allows for displaying or hiding the menu*/
    return {
        init: function(button, menu, page) {
            /*pass in either the menu itself or its container*/
            this.menuActive = false;
            this.button = button;
            this.menu = menu;
            this.page = page;
            /*this is a full-page overlay*/
            this.fullClose = document.createElement('div');
            this.fullClose.className = 'full_close';
            document.body.appendChild(this.fullClose);
            this.events();
        },
        events: function() {
            this.button.addEventListener('click', this.toggleMenu.bind(this), true);
            this.page.addEventListener('transitionend', this.transitionEndPage.bind(this), true);
            this.fullClose.addEventListener('click', this.toggleMenu.bind(this), true);
        },
        toggleMenu: function() {
            if (this.menuActive === false) {
                this.menu.classList.add('menu_show');
                this.page.classList.add('menu_active');
                this.button.classList.add('menu_active');
                this.fullClose.classList.add('menu_active');
                document.documentElement.classList.add('menu_active');
            }
            else {
                this.page.classList.remove('menu_active');
                this.button.classList.remove('menu_active');
                this.fullClose.classList.remove('menu_active');
                this.menu.classList.remove('menu_active');
            }
        },
        transitionEndPage: function(e) {
            if (e.target !== this.page) return false;
            if (this.menuActive === false) {
                this.menuActive = true;
                this.menu.classList.add('menu_active');
            }
            else {
                this.menuActive = false;
                document.documentElement.classList.remove('menu_active');
                this.menu.classList.remove('menu_show');
            }
        }
    };
};
var nzMenuToggleIe = function() {
    'use strict';
    var self;
    return {
        init: function(buttonEle, menuEle, supEle) {
            self = this;
            this.button = buttonEle;
            this.menu = menuEle;
            this.sup = supEle;
            this.menuActive = false;
            this.events();
        },
        events: function() {
            self.button.onclick = this.toggleMenu;
        },
        toggleMenu: function() {
            if (self.menuActive === false) {
                self.menu.style.display = 'block';
                self.menu.style.zIndex = '3';
                self.sup.style.zIndex = '3';
                self.menuActive = true;
            }
            else {
                self.menu.style.display = 'none';
                self.menu.style.zIndex = '0';
                self.sup.style.zIndex = '0';
                self.menuActive = false;
            }
        }
    };
};