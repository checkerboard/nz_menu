there are two components - the main functionality of the menu, and then the code to display or hide the menu itself.

### main functionality ("module A"): ###

the structure of nested lists of links should be reflected in the markup, i.e.:

<ul>
  <li>
      <a></a>
      <ul>
          <li>
              <a></a>
          </li>
      </ul>
  </li>
</ul>

this makes navigation with a screenreader possible and allows for a degraded non-js version.

pass in one list element.

styling:

all css selectors that relate to the main functionality ("module A") are prefixed with nz_.

to achieve the staggered animation effect, there are two "left" positions having the suffixes "_active_0" and "_active_1" in the css.  


### displaying or hiding the menu ("module B"): ###

this is independent of menu functionality. pass in (button, menu, page), where menu can be either the list element itself or a container for it.


all code is reasonably modern, targeting ie10+.  there are no libraries in use.


requirements:

https://developer.mozilla.org/en-US/docs/Web/API/Element/matches
https://developer.mozilla.org/en-US/docs/Web/API/Element/classList

various standard es5 features such as foreach and bind

i woe to pollute the code with monkey patches for old internet explorer.  i have added some rudimentary old fashioned code to toggle the menu, which you could turn on with ie conditional comments, perhaps with a view to discarding ie9 support further down the line.